!	 $eterna: @,v 1.2 2018/06/10 08:17:36 mrg Exp $
!
! Copyright (c) 2003-2018  Matthew Green.
!
! All rights reserved.  See the HELP IRCII COPYRIGHT file for more
! information.
!
Usage: @ <expression>
  The @ command evaluates the given expression in "variable
  expression" mode.  This is the same mode used inside IF ()
  and WHILE ().  Normally it is used for variable assignment
  as it provides a nicer syntax than ASSIGN does in some
  contexts.  These are the same thing:

	@ foo = [bar]
	assign foo bar

  as are these:

	@ foo = C
	assign foo $C

See Also:
  ASSIGN
  ALIAS
  IF
  SET INPUT_ALIASES
  ALIAS functions
