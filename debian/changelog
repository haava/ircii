ircii (20210314+really20190117-1) unstable; urgency=medium

  * QA upload.
  * Revert to previous release, because of freeze.
  * Add patch to Fix CVE-2021-29376 Closes: #986214

 -- Håvard Flaget Aasen <haavard_aasen@yahoo.no>  Sun, 30 May 2021 22:39:28 +0200

ircii (20210314-1) unstable; urgency=medium

  * QA Upload.
  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Changes Urgency by urgency in changelog file.

  * New upstream release.
    Fix (CVE-2021-29376). (Closes: #986214).
  * debian/control
    + Bump Standards-Version to 4.5.1. (no changes).
    + Bump Debhelper-compat to 13.
    + Add Rules-Requires-Root: no.
  * debian/patches
    + Refresh:
      + 0008-fix-spelling-error.diff
      + 0003-Add-ioption-to-local-include-paths-so-they-do-not-co.patch
      + 0004-absolute-path-for-motd-and-servers-file-and-other-de.patch
      + 0006-fix-some-spelling-errors.patch
  * debian/rules
    + Remove --as-needed linker flag.
  * debian/watch
    + Update to version 4.
  * Update copyright file.

 -- Daniel Echeverri <epsilon@debian.org>  Sun, 11 Apr 2021 11:19:42 -0500

ircii (20190117-1) unstable; urgency=medium

  * QA upload.
  * New upstream release.
  * debian/control
    + Bump Standards-Version to 4.3.0. (no changes).
    + Bump Debhelper compat to 12.
  * debian/patches
    + Refresh 0008-fix-spelling-error.diff patch.
    + Refresh 0003-Add-ioption-to-local-include-paths-so-they-do-not-co.patch.
    + Refresh 0004-absolute-path-for-motd-and-servers-file-and-other-de.patch.
    + Refresh 0006-fix-some-spelling-errors.patch.
    + Remove 0007-fix_security_problems.diff patch.
      + Merge with upstream.
  * debian/rules
    + inject CPPFLAGS to CFLAGS.
  * debian/compat
    + Switch compat level 11 to 12.

 -- Daniel Echeverry <epsilon@debian.org>  Wed, 20 Feb 2019 23:35:56 -0500

ircii (20170704-2) unstable; urgency=medium

  * QA upload.
  * Rebuild against gcc 8.
  * debian: Apply "wrap-and-sort -abst".
  * debian: Bump debhelper compat to v11.
  * debian/control:
    + Bump Standards-Version to 4.2.1.
    + Update Vcs-* fields and use repo under Salsa Debian group.
  * debian/gbp.conf: Update to fit DEP-14 convention.
  * debian/rules: Modernize instructions, use dh_missing --fail-missing.
  * debian/copyright: Use secure uri when possible.

 -- Boyuan Yang <byang@debian.org>  Thu, 13 Sep 2018 15:29:06 -0400

ircii (20170704-1) unstable; urgency=medium

  * QA Upload.
  * New upstream release.
  * debian/control
    + Change debhelper to 10 in B-D.
    + Bump Standards-Version to 4.0.0 (no changes).
  * debian/compat
    + Switch compat level 9 to 10.
  * debian/copyright
    + Extend copyright holders years.
    + Add missing copyright holders.
  * debian/patches
    + Refresh 0008-fix-spelling-error.diff
    + Refresh 0006-fix-some-spelling-errors.patch
    + Refresh 0007-fix_security_problems.diff

 -- Daniel Echeverry <epsilon77@gmail.com>  Mon, 10 Jul 2017 21:47:48 -0500

ircii (20151120-1) unstable; urgency=medium

  [ Daniel Echeverry ]
  * QA upload.
  * New upstream release. Closes: #747700
  * Update debian/watch file.
  * debian/control
    + Use HTTPS in Vcs-* fields
    + Bump Standards-Version to 3.9.7
    + Change debhelper to 9 in B-D.
    + Add libssl-dev in B-D.
  * debian/compat
    + Switch compat level 7 to 9.
  * debian/rules
    + Migrate to dh tiny rules.
    + Use Hardening flags.
  * debian/copyright:
    + Rewrite following copyright-format 1.0
  * debian/patches
    + Remove 0001-do-not-underline-period.patch
      + Merge with upstream
    + Remove 0005-avoid-crash-on-exiting-help.patch
      + Merge with upstream
    + Remove 0007-fix-segv-after-showing-motd.patch
      + Merge with upstream
    + Add 0007-fix_security_problems.diff
      + Fix some security errors to prevent FTBFS
    + Add 0008-fix-spelling-error.diff  patch
      + Fix some spelling errors in source code
    + Others patches are updated.
  * Update watch file. Closes: #721391
    + thanks  to Grant Bowman for the patch
  * Add ircii.docs file.
  * Update ircii.install file.
    + Now the documentation install from docs file
  * Add clean file
  * Use wrap-and-sort
  * update ircii.manpages file
    + Add wserv.1, ircio.1, ircflush.1 Closes: #721958

  [ Axel Beckert ]
  * Add debian/gbp.conf to be able to work on the package with gbp, too.
  * Update Tobias Klauser's e-mail address in debian/copyright and
    0003-Add-ioption-to-local-include-paths-so-they-do-not-co.patch.

 -- Daniel Echeverry <epsilon77@gmail.com>  Wed, 17 Feb 2016 11:13:36 -0500

ircii (20060725-1) unstable; urgency=low

  * QA upload
  * new upstream version (not newest but newest still having utf-8 support)
  * drop some modifications
  - those included upstream (or fixed otherwise)
  - no longer limit check against dcc sending files from /etc to novice mode
  * fix some spelling mistakes (to appease lintian)
  * modernize debian/rules
  - implement build-arch, build-indep
  - use dpkg-buildflags
  * if creating new /etc/irc/servers, put irc.debian.org in there
  * cherry-pick fix to not access server -1 (avoiding segv in some situations)
    (Closes: 175099, 258929, 175029, 175169, 163196)
  * add some more copyright holders and licenses to debian/copyright

 -- Bernhard R. Link <brlink@debian.org>  Mon, 07 Nov 2011 14:57:07 +0100

ircii (20051015-3) unstable; urgency=low

  * QA upload.
  * Migrate to source format 3.0 (quilt)
  * Include changes from NMU, thanks to Peter Eisentraut
  * Add a watch file but the upstream version does not currently build.
  * ircII goes reverse color when switching colour to on in xterms
    (Closes: #61008)
  * Fix period preceded by space and underlined in -c option
    documentation on ircII manual page (Closes: #577039)
  * Fix .Tp typo in manpage (lintian)

 -- Neil Williams <codehelp@debian.org>  Sat, 14 May 2011 17:06:26 +0100

ircii (20051015-2.3) unstable; urgency=low

  * Non-maintainer upload.
  * Moved purge rules from prerm to postrm (closes: #455020)

 -- Peter Eisentraut <petere@debian.org>  Sat, 05 Apr 2008 00:42:22 +0200

ircii (20051015-2.2) unstable; urgency=low

  * Non-maintainer upload from the Zurich BSP
  * Add -ioption to local include paths so they do not cover the system
    headers with the same name. This fixes errors/warnings about implicitly
    converted pointers. (Closes: #393547, #460342)

 -- Tobias Klauser <tklauser@access.unizh.ch>  Sat, 12 Jan 2008 22:30:02 +0100

ircii (20051015-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Apply a patch to fix a SEGV in mksiginc, thanks to brian m. carlson.
    (Closes: #377630)

 -- Philipp Kern <pkern@debian.org>  Sun, 21 Oct 2007 11:33:50 +0200

ircii (20051015-2) unstable; urgency=medium

  * fixed persisting input prompt (Closes: Bug#337406)
    This fix was provided by Bernhard R. Link
  * avoid crash on exiting /help (Closes: Bug#393158)
    This fix was provided by Bernhard R. Link
  * enable debugging info for Binaries (Closes: Bug#393142)
    This fix was provided by Bernhard R. Link
  * removed bashism in debian/rules (Closes: Bug#375481)
  * IRCNAME example in man page is fixed for some time (Closes: Bug#214139)
  * supports UTF-8 (Closes: Bug#335467)
  * new Version also fixed help segfault (Closes: Bug#66446)

 -- Bernd Eckenfels <ecki@debian.org>  Sun, 15 Oct 2006 19:44:02 +0200

ircii (20051015-1) unstable; urgency=low

  * oops, the last version did not provide utf8, this does.

 -- Bernd Eckenfels <ecki@debian.org>  Tue, 25 Oct 2005 00:04:54 +0200

ircii (20040820-2) unstable; urgency=low

  * oops, thats not the version which supports UTF-8 - sorry

 -- Bernd Eckenfels <ecki@debian.org>  Mon, 24 Oct 2005 23:56:57 +0200

ircii (20040820-1) unstable; urgency=low

  * new upstream version (Closes: Bug #335467)
  * new standards version
  * no config questions anymore (Closes: Bug #109996)

 -- Bernd Eckenfels <ecki@debian.org>  Mon, 24 Oct 2005 22:52:00 +0200

ircii (20030315-1) unstable; urgency=high

  * new upstream version, fixes security problem
    (buffer overflow exploitable by servers as reported in #188223)
  * standards version 3.5.6.0 -> 3.5.9.0 (DEB_BUILD_OPTIONS Support added)

 -- Bernd Eckenfels <ecki@debian.org>  Mon, 21 Apr 2003 19:51:33 +0200

ircii (20020912-2) unstable; urgency=low

  * increased help buffer (Closes: Bug #66446)
  * specify override for += -s/-g for DEB_BUILD_OPTIONS

 -- Bernd Eckenfels <ecki@debian.org>  Fri, 04 Oct 2002 02:54:37 +0200

ircii (20020912-1) unstable; urgency=low

  * new upstream version

 -- Bernd Eckenfels <ecki@debian.org>  Tue, 17 Sep 2002 08:47:45 +0200

ircii (20020403-2) unstable; urgency=low

  * removed *.orig files
  * ensure sane permission of ircII scripts

 -- Bernd Eckenfels <ecki@debian.org>  Mon, 09 Sep 2002 06:34:35 +0200

ircii (20020403-1) unstable; urgency=low

  * new upstream
  * little less verbose postinst
  * moved to main/net from non-US

 -- Bernd Eckenfels <ecki@debian.org>  Mon, 09 Sep 2002 05:22:44 +0200

ircii (20020322-1) unstable; urgency=low

  * new upstream, fixing a lot of bugs (#118566, #30035, #51144, #87578,
    #71611, #72799, #79628, #103469, #113884, #118107, #55396)
  * irc_path does not contain ~ anymore (Closes: #132339)
  * upgraded from 3.1.0 to 3.5.6.0 (added DEB_)

 -- Bernd Eckenfels <ecki@debian.org>  Tue, 26 Mar 2002 19:02:22 +0100

ircii (20010612-4) unstable; urgency=low

  * fixed speling of urgency field in changelog (here) (Closes: 107981)

 -- Bernd Eckenfels <ecki@debian.org>  Wed,  8 Aug 2001 02:55:50 +0200

ircii (20010612-3) unstable; Urgency=low

  * added resize fix bug from matthew (Closes: 107763)

 -- Bernd Eckenfels <ecki@debian.org>  Mon,  6 Aug 2001 19:20:10 +0200

ircii (20010612-2) unstable; Urgency=low

  * fixed build dependency on curses (Closes: #107110)

 -- Bernd Eckenfels <ecki@debian.org>  Tue, 31 Jul 2001 01:18:51 +0200

ircii (20010612-1) unstable; Urgency=low

  * new upstream version, may fix a lot of bugs (colour)

 -- Bernd Eckenfels <ecki@debian.org>  Sun, 29 Jul 2001 00:44:19 +0200

ircii (4.4Z-3) unstable; Urgency=high

  * Security: this release will remove . from the LOAD_PATH. Note: it
    still has "~/.irc" as the first element to actually allow to overwrite
    system scripts. Thanks to Sami Liedes.
  * fixed bashism in prerm (make lintian more happy)
  * strip binaries with install -s (make lintian even more happy)

 -- Bernd Eckenfels <ecki@debian.org>  Mon, 12 Feb 2001 03:13:48 +0100

ircii (4.4Z-2) unstable; Urgency=low

  * added strmcpy patch to keep it from segfaulting from David Murn
    fixes bug #72799. Reported Upstream.

 -- Bernd Eckenfels <ecki@debian.org>  Sat, 30 Sep 2000 02:35:17 +0200

ircii (4.4Z-1) unstable; Urgency=low

  * new upstream version, closes bug #71496

 -- Bernd Eckenfels <ecki@debian.org>  Wed, 13 Sep 2000 02:10:55 +0200

ircii (4.4U-2) unstable; Urgency=low

  * new upstream version
  * includes IPv6 support now!
  * fixes bug #66297 and #65217

 -- Bernd Eckenfels <ecki@debian.org>  Sat,  1 Jul 2000 14:43:01 +0200

ircii (4.4M-1) frozen unstable; Urgency=high

  * fixed dcc chat exploit from bladi & aLmUDeNa
  * still contains noinfect patch and additional support for absolute path
    names of server and motd file
  * no ipv6 support in the frozen tree, yet

 -- Bernd Eckenfels <ecki@debian.org>  Sun, 12 Mar 2000 03:35:28 +0100

Old Changelog:

ircii (4.4L-6) unstable; Urgency=low, Closes=35198

  * upstream colour support is activated by SET COLOUR (#50521)

 -- Bernd Eckenfels <ecki@debian.org>  Sat, 20 Nov 1999 19:37:26 +0100

ircii (4.4L-5) unstable; Urgency=low, Closes=35198

  * use protocols/talkd.h (#35198)

 -- Bernd Eckenfels <ecki@debian.org>  Wed,  3 Nov 1999 03:46:36 +0100

ircii (4.4L-4) unstable; Urgency=low, Closes=37097 44700

  * reupload because of missing source in first try to non-us

 -- Bernd Eckenfels <ecki@debian.org>  Sun,  3 Oct 1999 23:07:39 +0200

ircii (4.4L-3) unstable; Urgency=low, Closes=37097 44700

  * remove old link in /isr/lib/irc/ircII.servers before any other checks

 -- Bernd Eckenfels <ecki@debian.org>  Sat, 25 Sep 1999 23:08:11 +0200

ircii (4.4L-2) unstable; Urgency=low, Closes=37097 44700

  * fixed bug in my bufffer-overflow fix which made /on fail

 -- Bernd Eckenfels <ecki@debian.org>  Sat, 25 Sep 1999 21:13:54 +0200

ircii (4.4L-1) unstable; urgency=low, closes=#37097 44700

  * new upstream version
  * moved data files to /usr/share/ircII/
  * as novice /etc/ and *passwd* are restricted with some verbose comments
  * moved to non-US because of cast.c (obtained from a european site)
  * core dump fix in output of motd (still missing upstream)
  * absolute path for motd and servers file
  * no-infect patch
  * violating the Policy (/usr/doc)!
  * provides virtual package irc (#37097)

 -- Bernd Eckenfels <ecki@debian.org>  Mon, 13 Sep 1999 23:54:03 +0200

ircii (4.4-5) unstable; urgency=low, closes=31451 36563 37430 35077 24541 24767

  * test for /etc/ or passwd only as novice (#31451)
  * recompiled with recent libs (#36563, #37430)
  * remove alternatives for executable and man (#35077)
  * move wserv, ircio, ircflush to /usr/sbin
  * changed configure script to find sendmail in /usr/sbin
  * changed configure script to find nonblocking io (may fix #24541, #24767)
  * (wserv works in xterm and screen)

 -- Bernd Eckenfels <ecki@debian.org>  Thu, 13 May 1999 02:45:15 +0200

ircii (4.4-4) unstable; urgency=high

  * recompiled with the new ncurses (hopefully)
  * fixed -geom bug for rxvt (#20867)
  * new standards version, renamed upstream changelog

 -- Bernd Eckenfels <ecki@debian.org>  Sun,  1 Nov 1998 02:53:12 +0100

ircii (4.4-3) frozen; urgency=high

  * Security Fix for DCC buffer overflow (noinfect)
  * Security Fix for not flushing Linux Console in DCC CHAT (flashfix)
  * new Copyright notice (#21683)
  * fixed rules clean if no makefile is present

 -- Bernd Eckenfels <ecki@debian.org>  Thu, 11 Jun 1998 20:55:37 +0200

ircii (4.4-2) frozen; urgency=high

  * Security Fix for filtering non-printables (Bug#19743)
  * mIrc Color Support fixed (Bug#19583)

 -- Bernd Eckenfels <ecki@debian.org>  Mon, 16 Mar 1998 22:00:41 +0100

ircii (4.4-1) unstable; urgency=low

  * new upstream version with applied contrib/colour patch
  * /etc/irc/script/local is now recognized as a config file (#11559)
  * fixed rules file to include binary-indep and binary-arch targets (#14309)
  * Will install prerm script excecutable, now (#14309)
  * Libc6 compile.
  * new default irc server is irc.debian.org
  * make /usr/bin/irc an alternative to /usr/bin/ircII

 -- Bernd Eckenfels <ecki@debian.org>  Sun,  8 Feb 1998 15:28:01 +0100

ircii (2.9.3roof-1.1) unstable; urgency=low

  * Non-maintainer release.
  * Libc6 compile.

 -- Martin Mitchell <martin@debian.org>  Tue, 28 Oct 1997 03:54:17 +1100

ircii (2.9.3roof-1) unstable; urgency=low

  * New maintainer. (old: Andrew Howell <andrew@it.com.au>)
  * Updated to Standards-Version 2.1.1.0.
  * new upstream version
  * This fixes the /leave core dump (#4504)
  * fixed a (reported) syntax error in Makefile.in
  * fixed some compiler warnings about tputs in ircterm.h for linux
  * search scripts in /etc/irc/script, too.
  * servers file in /etc/irc/servers, motd in /etc/irc/motd
  * core dump on empty line in motd fixed (reported)
  * -DDEBIAN for all the changes
  * ircio does not work :(

 -- Bernd Eckenfels <ecki@debian.org>  Wed, 20 Nov 1996 23:03:11 +0100

ircii (2.8.16beta-1) stable; urgency=low

  * Taken over package from Carl Streeter.
  * Upgraded to latest upstream version.
  * Removed Package_Revision from control file.
  * Added Architecture field to control file.
  * ELF binary.
  * Postinst queries you for irc server now.
  * Fixed Bug#981: irc package should ask for default server
  * Fixed Bug#1173: ircII bug? ircII.servers
  * Fixed Bug#1804: manpage of irc missing

 -- Andrew Howell <andrew@it.com.au> 960419

Irc 2.8.2 Debian 1 - 5/11/95 Carl Streeter <streeter@cae.wisc.edu>

  * Initial release.

Local variables:
mode: debian-changelog
End:
