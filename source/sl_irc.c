/*
 * sl_irc.c: ircII extras on top of sl.c.
 *
 * Written By Matthew R. Green
 *
 * Copyright (c) 2017 Matthew R. Green.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote products
 *    derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include "irc.h"
IRCII_RCSID("@(#)$eterna: sl_irc.c,v 1.3 2017/07/09 06:45:33 mrg Exp $");

#include "sl_irc.h"
#include "ircaux.h"

u_char *
sl_concat(StringList *sl, u_char *sep)
{
	size_t i;
	size_t sep_len = strlen(CP(sep));
	size_t len = 0;
	u_char *str;

	for (i = 0; i < sl->sl_cur; i++)
	{
		if (i != 0)
			len += sep_len;
		len += strlen(sl->sl_str[i]) + sep_len;
	}
	str = new_malloc(len + 1);
	str[0] = '\0';
	for (i = 0; i < sl->sl_cur; i++)
	{
		if (i != 0)
			my_strncat(str, sep, len);
		my_strncat(str, sl->sl_str[i], len);
	}
	str[len] = '\0';

	return str;
}

size_t
sl_size(StringList *sl)
{
	return sl->sl_cur;
}

u_char *
sl_iter_fwd(StringList *sl, size_t *next)
{
	size_t	this = *next;

	if (this >= sl->sl_cur)
		return NULL;
	*next += 1;
	return UP(sl->sl_str[this]);
}

u_char *
sl_iter_rev(StringList *sl, size_t *next)
{
	size_t	this = *next;

	if (this >= sl->sl_cur)
		return NULL;
	*next += 1;
	return UP(sl->sl_str[sl->sl_cur - this - 1]);
}
